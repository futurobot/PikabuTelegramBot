using System;

namespace PikabuTelegramBot
{
    public class PostItem
    {
        public string Type { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }

        public override string ToString()
        {
            return $"Type: {Type} Title: {Title} Url: {Url} ImageUrl: {ImageUrl} Descriptopn: {Description}";
        }

    }
}