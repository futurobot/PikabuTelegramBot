using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reactive.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Parser.Html;

namespace PikabuTelegramBot
{
    public class ApiLikeThing
    {
        private readonly long HTML_CACHE_TIME = 60 * 60;  //1200 seconds = one hour
        private readonly long POSTS_CACHE_TIME = 7 * 1440 * 60;  //one week

        private readonly HttpClient _httpClient;
        private readonly string[] _randomPartitions = new string[] { "hot", "best", "best/week", "best/month", "new" };
        private TimeCacheDictionary<string, PostItem> _postCache = new TimeCacheDictionary<string, PostItem>();
        private TimeCacheDictionary<string, string> _htmlCache = new TimeCacheDictionary<string, string>();
        private Random _randomizer = new Random(DateTime.UtcNow.Millisecond);

        static ApiLikeThing()
        {
            //Register encoding core pages
            System.Text.EncodingProvider encodeProvider;
            encodeProvider = System.Text.CodePagesEncodingProvider.Instance;
            Encoding.RegisterProvider(encodeProvider);
        }

        public ApiLikeThing()
        {
            //Create http client
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri("http://pikabu.ru");
        }

        public IObservable<List<PostItem>> Top(int count = 3)
        {
            return Observable.FromAsync(async () =>
            {
                var html = await RequestHtml("hot");
                var postsUrl = await ParsePostsUrl(html);
                var list = new List<PostItem>();
                foreach (var url in postsUrl.Take(count))
                {
                    var post = await GetPost(url);
                    list.Add(post);
                }
                return list;
            });
        }

        public IObservable<List<PostItem>> Search(string query, int count = 3)
        {
            return Observable.FromAsync(async () =>
            {
                var html = await RequestHtml($"search.php?q={query}");
                var postsUrl = await ParsePostsUrl(html);
                var list = new List<PostItem>();
                foreach (var url in postsUrl.Take(count))
                {
                    var post = await GetPost(url);
                    list.Add(post);
                }
                return list;
            });
        }

        public IObservable<List<PostItem>> Tags(string tagname, int count = 3)
        {
            return Observable.FromAsync(async () =>
            {
                var html = await RequestHtml($"search.php?t={tagname}");
                var postsUrl = await ParsePostsUrl(html);
                var list = new List<PostItem>();
                foreach (var url in postsUrl.Take(count))
                {
                    var post = await GetPost(url);
                    list.Add(post);
                }
                return list;
            });
        }

        public IObservable<PostItem> Random(int amount = 3)
        {
            int finalAmount = Math.Max(0, Math.Min(amount, 10));
            return Observable.FromAsync(async () =>
            {
                var path = _randomPartitions[_randomizer.Next(_randomPartitions.Length - 1)];
                var page = _randomizer.Next(1, 20);
                var html = await RequestHtml($"{path}?page={page}");
                var postsUrl = await ParsePostsUrl(html);
                var url = postsUrl[_randomizer.Next(postsUrl.Count - 1)];
                return await GetPost(url);
            });
        }



        private async Task<string> RequestHtml(string path)
        {
            string html = _htmlCache.Get(path);
            if (html != null)
            {
                return html;
            }
            else
            {
                var response = await _httpClient.GetAsync(path);
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new HttpRequestException($"Status code is {response.StatusCode}");
                }
                html = Encoding.GetEncoding(response.Content.Headers.ContentType.CharSet).GetString(await response.Content.ReadAsByteArrayAsync());
                _htmlCache.Put(path, html, HTML_CACHE_TIME);
                return html;
            }
        }

        private async Task<List<string>> ParsePostsUrl(string html)
        {
            var document = await new HtmlParser().ParseAsync(html);
            var storiesElement = document.QuerySelector("div.stories");
            return storiesElement.QuerySelectorAll("a.story__title-link").Select(e => e.GetAttribute("href")).Take(20).ToList();
        }

        private async Task<PostItem> GetPost(string url)
        {
            var post = _postCache.Get(url);
            if (post != null)
            {
                return post;
            }
            else
            {
                var html = await RequestHtml(url);
                post = await ParsePost(html);
                _postCache.Put(url, post, POSTS_CACHE_TIME);
                return post;
            }
        }

        private async Task<PostItem> ParsePost(string html)
        {
            var document = await new HtmlParser().ParseAsync(html);
            return new PostItem
            {
                Type = document.QuerySelector("meta[property='og:type']")?.GetAttribute("content"),
                Url = document.QuerySelector("meta[property='og:url']")?.GetAttribute("content"),
                ImageUrl = document.QuerySelector("meta[property='og:image']")?.GetAttribute("content"),
                Title = document.QuerySelector("meta[property='og:title']")?.GetAttribute("content"),
                Description = document.QuerySelector("meta[property='og:description']")?.GetAttribute("content")
            };
        }
    }
}