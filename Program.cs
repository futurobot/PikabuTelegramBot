﻿using System;
using Microsoft.Extensions.Configuration;
using static PikabuTelegramBot.PikabuBot;

namespace PikabuTelegramBot
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false)
                .Build();

            var bot = new PikabuBot(new Configuration
                {
                    BotName = config.GetValue<string>("name"),
                    BotToken = config.GetValue<string>("bot_token"),
                    BotanToken = config.GetValue<string>("botan_token")
                }
            );
            bot.Run();
        }
    }
}
