using System;
using System.Collections.Generic;

namespace PikabuTelegramBot
{
    public class TimeCacheDictionary<K, V>
    {
        private Dictionary<K, V> _valueDictionary = new Dictionary<K, V>();
        private Dictionary<K, double> _timeDictionary = new Dictionary<K, double>();

        public void Put(K key, V val, double expireTimeSeconds = 0)
        {
            Remove(key);
            _valueDictionary.Add(key, val);
            _timeDictionary.Add(key, TimeSpan.FromTicks(DateTime.UtcNow.Ticks).TotalSeconds + expireTimeSeconds);
        }

        public V Get(K key)
        {
            if (_timeDictionary.ContainsKey(key))
            {
                if (TimeSpan.FromTicks(DateTime.UtcNow.Ticks).TotalSeconds > _timeDictionary[key])
                {
                    _valueDictionary.Remove(key);
                    _timeDictionary.Remove(key);
                    return default(V);
                }
                else
                {
                    return _valueDictionary[key];
                }
            }
            return default(V);
        }

        public void Remove(K key)
        {
            if (_valueDictionary.ContainsKey(key))
            {
                _valueDictionary.Remove(key);
                _timeDictionary.Remove(key);
            }
        }
    }
}